Run Elm app locally:

```.bash
elm reactor
```

Build Elm app:

```.bash
elm make src/Main.elm --optimize --output=elm.js
```

Build and minify (requires `uglify-js` from npm)

```.bash
./build-min.sh src/Main.elm
```

# Build and push

Install:

Install docker and docker-buildx. Then run the following command:

```
docker run --privileged --rm tonistiigi/binfmt --install all
```

```
Arm

```
docker buildx build --platform linux/arm/v7 -t yen3k/afterwork-website --push .
